<?php
/**
 * The template for displaying page about
 *
 * @author Botez Costin
 * @package Blue_Barn
 */

get_header(); ?>
    <div class="container_12">
    	<div class="grid_12 alpha omega" id="content-wrapper">
        	<div id="content-top">&nbsp;</div>
        	<div id="content" class="clearfix">
				<div id="tabs-container" class="clearfix">
					<div class="images-frame clearfix"></div>
				  	<div class="images">
				    	<div>
				      		<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food/tipdyfood_20100723_8232__medium.png" />
				    	</div>
			    		<div>
				      		<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food/tipsyfood_20100723_7908__medium.png" />
				    	</div>
				    	<div>
				      		<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food/tipsyfood_20100723_8053__medium.png" />
				    	</div>
				    	<div>
				      		<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/food/tipsyfood_20100723_8181__medium.png" />
				    	</div>
				  	</div>
				  	<div class="tabs">
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
  					</div>
  					<hr class="space"/>
				  	<div id="backward-forward" class="clearfix">
				    	<a class="backward">&laquo;</a>
				    	<a class="forward">&raquo;</a>
				  	</div>
				</div>

				<div id="content-text-wrapper">
					<?php global $post; echo $post->post_content; ?>
				</div>
        	</div>
        	<div id="content-bottom">&nbsp;</div>
      	</div>
    </div>

<?php
get_footer();
