<?php
/**
 * The template for displaying page about
 *
 * @author Botez Costin
 * @package Blue_Barn
 */

get_header(); ?>
	<div class="container_12">
    	<div class="grid_12 alpha omega" id="content-wrapper">
        	<div id="content-top">&nbsp;</div>
        	<div id="content" class="clearfix">
				<div id="content-text-wrapper">
					<?php global $post; echo $post->post_content; ?>
				</div>
				<div class="embed">
				 	<link href="http://www.opentable.com/ism/feed.css" rel="stylesheet" type="text/css" />
				 	<div id="OT_searchWrapperAll">
				 		<script type="text/JavaScript" src="http://www.opentable.com/ism/?rid=28717"></script>
				 		<noscript id="OT_noscript">
				 			<a href="http://www.opentable.com/single.aspx?rid=28717&restref=28717&rtype=ism"><?php _e('Reserve Now on OpenTable.com', 'thetipsypig'); ?></a>
				 		</noscript>
					 	<div id="OT_logoLink">
					 		<a href="http://www.opentable.com/single.aspx?rid=28717&restref=28717&rtype=ism"><?php _e('The Tipsy Pig', 'thetipsypig'); ?></a>
					 		Reservations
					 	</div>
					 	<div id="OT_logo">
					 		<a href="http://www.opentable.com/home.aspx?restref=28717&rtype=ism" title="Powered By OpenTable">
					 			<img src="http://www.opentable.com/img/buttons/Otlogo.gif" id="OT_imglogo" alt="Restaurant Management Software" />
					 		</a>
					 	</div>
				 	</div>
				</div>
        	</div>
        	<div id="content-bottom">&nbsp;</div>
      	</div>
    </div>
<?php
get_footer();
