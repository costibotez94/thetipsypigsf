<?php
/**
 * The template for displaying page about
 *
 * @author Botez Costin
 * @package The Tipsy Pig
 */

get_header(); ?>

	<div class="container_12">
    	<div class="grid_12 alpha omega" id="content-wrapper">
        	<div id="content-top">&nbsp;</div>
        	<div id="content" class="clearfix">
				<div id="tabs-container" class="clearfix">
  					<div class="images-frame clearfix"></div>
  					<div class="images">
					    <div>
					    	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/tipsy_food_20100811_9185_medium.jpg" />
					    </div>
					    <div>
					    	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/tipsyfood_20100723_8063_medium.jpg" />
					    </div>
					    <div>
					      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/tipsy_food_20100811_9197_medium.jpg" />
					    </div>
					    <div>
					    	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/tipsy_food_20100811_9188_medium.jpg" />
					    </div>
					    <div>
					      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/4054903512_abaeaf2dc3_medium.jpg" />
					      	<p class="caption"><?php _e('The Pig', 'thetipsypig'); ?></p>
					    </div>
    					<div>
					    	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/tipsy_food_20100811_9085_medium.jpg" />
					      	<p class="caption"><?php _e('Merriment', 'thetipsypig'); ?></p>
					    </div>
					    <div>
					      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/tipsy_wide_3_medium.jpg" />
					      	<p class="caption"><?php _e('The Garden', 'thetipsypig'); ?></p>
					    </div>
					    <div>
					      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/img_9424_medium.jpg" />
					      	<p class="caption"><?php _e('The Library', 'thetipsypig'); ?></p>
					    </div>
					    <div>
					      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/3978837391_c640719bf5_b_medium.jpg" />
					      	<p class="caption"><?php _e('The Parlor', 'thetipsypig'); ?></p>
					    </div>
    					<div>
				      		<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/3979544116_21d10b106d_b_medium.jpg" />
				      		<p class="caption"><?php _e('The Living Room', 'thetipsypig'); ?></p>
				    	</div>
				    	<div>
				      		<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/img_7534_medium.jpg" />
				      		<p class="caption"><?php _e('The Tipsy Pig', 'thetipsypig'); ?></p>
				    	</div>
				  	</div>
				  	<div class="tabs">
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
  					</div>
  					<hr class="space"/>
  						<div id="backward-forward" class="clearfix">
					    	<a class="backward">&laquo;</a>
					    	<a class="forward">&raquo;</a>
					  	</div>
				</div>
				<div id="content-text-wrapper">
					<h3><?php _e('Welcome to the Tipsy Pig!', 'thetipsypig'); ?></h3>
					<p><?php _e('The Tipsy Pig is an American Gastrotavern and home away from home.  We pride ourselves on a large and unique beer selection as well as an ever-changing seasonal gastro menu.  Recently awarded the distinction of Top 10 New Restaurants &amp; Top 100 Restaurants in the Bay Area by Michael Bauer, we love what we do everyday and love watching you all enjoy in merriment.', 'thetipsypig'); ?></p>
					<p><?php _e('We are open 7 days a week for dinner, beginning at 5p, last dinner seating is between 9:30pm - 10pm. Brunch is served Friday - Sunday at 11am with last seating at 2:45pm. The bar is open late each night so we look forward to seeing you at The Tipsy Pig.', 'thetipsypig'); ?></p>
					<p><?php _e('Oink!' ,'thetipsypig'); ?></p>
					<div class="a-little-note-wrapper clearfix">
				  		<div class="a-little-note-top"></div>
				  		<div class="a-little-note">
				    		<img src="<?php echo get_template_directory_uri(); ?>/images/a-little-note-text-trans.png" alt="A little note" class="a-little-note-text" />
				    		<p><?php _e('The Pig is open for brunch Friday - Sunday, doors open at 11am. &nbsp;Also, please join us for Happy Hour Monday - Thursday, half priced bottles of wine 5pm-7pm, to go along with our discounted bites menu served from &nbsp;530pm to 630pm, offering our favorite Tipsy classics.', 'thetipsypig'); ?></p>
				  		</div>
  						<div class="a-little-note-bottom"></div>
					</div>
				</div>
    			<div class="press">
	      			<h3><?php _e('Press &amp; Reviews...', 'thetipsypig'); ?></h3>
					<ul>
						<li><a href="http://tlc.discovery.com/videos/food-buddha-one-of-each.html"><?php _e('The Tipsy Pig was just featured on TLC\'s newest show Food Buddha.', 'thetipsypig'); ?></a></li>
						<li><a href="http://www.sfgate.com/cgi-bin/listings/restaurants/venuetop2010?vid=602559"><?php _e('We are very proud to have been included in Michael Bauer\'s Top 100 Bay Area Restaurants', 'thetipsypig'); ?></a></li>
						<li><a href="http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2009/12/27/FDMT1B11T8.DTL"><?php _e('The Pig was named as one of the Top 10 New Restaurants by M. Bauer &amp; The SF Chronicle', 'thetipsypig'); ?></a></li>
						<li><a href="/press_examiner_041709.pdf"><?php _e('Patricia Unterman\'s &nbsp;SF Examiner Review', 'thetipsypig'); ?></a></li>
						<li><a href="http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2009/04/26/FDSA17265U.DTL&amp;type=food"><?php _e('Michael Bauer\'s Review calls The Tipsy Mac &amp; Cheese "the best on the planet"', 'thetipsypig'); ?></a></li>
						<li><a href="http://www.sfgate.com/cgi-bin/article.cgi?f=/c/a/2010/02/07/FDB81BMKJ0.DTL#recipe1"><?php _e('The Chronicle kitchen breaks down the Tipsy Pig Mac &amp; Cheese recipe.&nbsp;', 'thetipsypig'); ?></a></li>
						<li><a href="http://www.youtube.com/watch?v=EeBVdT0XCGk"><?php _e('A little video to help you get to know us' ,'thetipsypig'); ?></a></li>
					</ul>
    			</div>
        	</div>
        	<div id="content-bottom">&nbsp;</div>
      	</div> <!-- #content-wrapper -->
    </div> <!-- #containter_12 -->

<?php
get_footer();
