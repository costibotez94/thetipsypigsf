<?php
/**
 * The template for displaying page about
 *
 * @author Botez Costin
 * @package Blue_Barn
 */

get_header();?>
	<div class="container_12">
    	<div class="grid_12 alpha omega" id="content-wrapper">
        	<div id="content-top">&nbsp;</div>
        	<div id="content" class="clearfix">
				<style>
					hr { border-color: white; }
					p { font-size: 1.25em; font-weight: bold; }
				</style>
				<div id="content-text">
					<?php global $post; echo $post->post_content; ?>
		        </div>
	        	<div id="content-bottom">&nbsp;</div>
      		</div>
    	</div>
  	</div>
<?php
get_footer();
