(function($){

  $(document).ready(function(){
    /*
     *   = Galleries
     *
     *   If the gallery has the '.paginated' class then it
     *   looks for element with an id of $GALLERY_NAME-pagination
     *   and will update that with "#n of #total" as the user
     *   pages through
     *
     *   If the gallery has the '.captioned' class then it looks
     *   for an element of $GALLERY_NAME-caption and updates
     *   it with the title of the current image as the user pages
     *   through.
     *
     *   Finally, if the gallery has the class '.thumbnails' then it
     *   looks for an element of $GALLERY_NAME-thumbnails and switches
     *   the current item for the selected one
     */
    $(".scrollable").each(function(){
      var self       = $(this),
          scrollable = self.scrollable({
            next: '#'+this.id+'-next',
            prev: '#'+this.id+'-prev',
            circular: true
          });

      if (self.hasClass('autoplay')) {
        scrollable.autoscroll({ autoplay: true });
      }

      self.bind("onSeek",function(e){
        var s    = self.data('scrollable'),
            idx  = s.getIndex(),
            item = s.getItems()[idx];
        if (self.hasClass( 'paginated' )) {
          var curPage = $('#'+self[0].id+'-pagination'),
              txt     = curPage.text();
          txt = txt.replace(/^(\d+)/, idx+1);
          txt = txt.replace(/(\d+)$/, s.getSize());
          curPage.text(txt);
        }
        if(self.hasClass( 'captioned' )) {
          $('#'+self[0].id+'-caption').text(item ? item.title : '');
        }
      });

      if (self.hasClass('thumbnails')) {
        $('#'+this.id+'-thumbnails a').live('click',function(e){
          e.preventDefault();
          var thumb = $(this),
              s     = self.data('scrollable'),
              idx   = null;
          // Find the corresponding large item in the stack
          $.each(s.getItems(),function(i,v){
            if ($(v).attr('src') == thumb.attr('href')) { idx = i; }
          });
          if (idx !== null) { s.seekTo(idx, 100); }
        });
      }
    });

  });
})(jQuery);
