<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package The Tipsy Pig
 */

?>

	</div><!-- #content-outer-wrapper -->
	<div id="footer-wrapper" class="clearfix">
    	<div class="container_12">
      		<div class="grid_12 alpha omega" id="footer">
        		<div class="grid_4 alpha omega" id="hours">
            		<h3><?php _e('HOURS OF OPERATION', 'thetipsypig'); ?></h3>
            		<p><?php _e('OPEN 7 DAYS A WEEK', 'thetipsypig'); ?></p>
            		<p><strong><?php _e('Monday thru Thursday', 'thetipsypig'); ?></strong><br/>- <?php _e('5PM onward', 'thetipsypig'); ?></p>
            		<p><strong><?php _e('Friday thru Sunday' ,'thetipsypig'); ?></strong><br/>- <?php _e('Brunch/Lunch Starts at 11AM' ,'thetipsypig'); ?></p>
        		</div>
        		<div class="grid_8 alpha omega">
          			<img src="<?php echo get_template_directory_uri(); ?>/images/footer-trans.png" usemap="#footer-image-map" />
        		</div>
    			<div class="grid_12 alpha omega">
          			<p>&copy; <?php _e('2010 The Tipsy Pig', 'thetipsypig'); ?> &reg;</p>
		          	<map id="footer-image-map" name="footer-image-map">
		            	<area shape="rect" alt="Email Us" title="" coords="188,49,432,72" href="mailto:thepig@thetipsypigsf.com" target="" />
		            	<area shape="rect" alt="The Tipsy Pig on Facebook" title="The Tipsy Pig on Facebook" coords="187,75,434,94" href="http://www.facebook.com/pages/San-Francisco-CA/The-Tipsy-Pig/78955616105" target="" />
		            	<area shape="rect" alt="The Tipsy Pig on Twitter" title="The Tipsy Pig on Twitter" coords="187,97,435,120" href="http://twitter.com/tipsypig" target="" />
		            	<area shape="rect" alt="Subscribe to our newsletter" title="Subscribe to our newsletter" coords="179,123,444,148" href="http://www.mailboto.com/templates/v415/tipsypig_subscribe.html" target="" />
		            	<area shape="rect" alt="Map" title="Map" coords="455,11,655,192" href="http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=+The+Tipsy+Pig+2231+Chestnut+Street+San+Francisco,+CA+94123+&sll=37.871593,-122.272747&sspn=0.149329,0.308647&ie=UTF8&hq=The+Tipsy+Pig&hnear=2231+Chestnut+St,+San+Francisco,+CA+94123&ll=37.800595,-122.440009&spn=0.009342,0.01929&z=16&iwloc=A" target="" />
		          	</map>
        		</div>
      		</div>
    	</div>
	</div>
	<script type="text/javascript">
	var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
	document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
	try {
	var pageTracker = _gat._getTracker("UA-9755745-1");
	pageTracker._trackPageview();
	} catch(err) {}</script>


<?php wp_footer(); ?>

</body>
</html>
