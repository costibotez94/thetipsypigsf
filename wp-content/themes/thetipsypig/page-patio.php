<?php
/**
 * The template for displaying page about
 *
 * @author Botez Costin
 * @package Blue_Barn
 */

get_header(); ?>
	<div class="container_12">
    	<div class="grid_12 alpha omega" id="content-wrapper">
        	<div id="content-top">&nbsp;</div>
        	<div id="content" class="clearfix">
				<div id="tabs-container" class="clearfix">
  					<div class="images-frame clearfix"></div>
  					<div class="images">
					    <div>
					    	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/tipsy_wide_3_medium.jpg" />
					    </div>
					    <div>
					      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/tipsy_zoom_1_medium.jpg" />
					    </div>
					    <div>
					      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/tipsy_zoom_2_medium.jpg" />
					    </div>
					    <div>
					      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/cimg2169.jpg_medium.jpeg" />
					    </div>
					    <div>
					      	<img src="<?php echo get_template_directory_uri(); ?>/inc/uploads/tipsy_wide_4_medium.jpg" />
					    </div>
				  	</div>
				  	<div class="tabs">
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
					    <a href="#"></a>
  					</div>
  					<hr class="space"/>
				  	<div id="backward-forward" class="clearfix">
				   		<a class="backward">&laquo;</a>
				    	<a class="forward">&raquo;</a>
				  	</div>
				</div>
				<div id="content-text-wrapper">
					<?php
						global $post; echo $post->post_content;
					?>
				</div>
        	</div>
        	<div id="content-bottom">&nbsp;</div>
      	</div>
    </div>

<?php
get_footer();
