<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package The Tipsy Pig
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="clearfix" id="header-wrapper">
		<div class="container_12">
      <div class="grid_12 alpha omega" id="header">
      			<h1><?php _e('The Tipsy Pig', 'thetipsypig'); ?></h1>
        		<p><?php _e('2231 Chestnut Street, San Francisco, CA 94123, (415) 675-9876', 'thetipsypig'); ?></p>
      		</div>
      		<div class="grid_12 alpha omega" id="navigation">
	      		<ul>
              <li id="home"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/home-trans.png" alt="Home"/></a></li>
              <li id="menus"><a href="<?php echo home_url('menus'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/menus-trans.png" alt="Menus"/></a></li>
              <li id="reservations"><a href="<?php echo home_url('reservations'); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/images/reservations-trans.png" alt="Reservations"/>
              </a></li>
              <li id="patio"><a href="<?php echo home_url('patio'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/patio-trans.png" alt="Patio"/></a></li>
              <li id="shop"><a href="<?php echo home_url('shop'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/shop-trans.png" alt="Shop"/></a></li>
            </ul>
      		</div>
      	</div>
     </div>
	<div id="content-outer-wrapper" class="clearfix">
