$(document).ready(function() {
  $("#home a img").hover(function(){
    $(this).attr("src", "../wp-content/themes/thetipsypig/images/home-active-trans.png");
  },function(){
    $(this).attr("src", "../wp-content/themes/thetipsypig/images/home-trans.png");
  });
  $("#menus a img").hover(function(){
    $(this).attr("src", "../wp-content/themes/thetipsypig/images/menus-active-trans.png");
  },function(){
    $(this).attr("src", "../wp-content/themes/thetipsypig/images/menus-trans.png");
  });
  $("#reservations a img").hover(function(){
    $(this).attr("src", "../wp-content/themes/thetipsypig/images/reservations-active-trans.png");
  },function(){
    $(this).attr("src", "../wp-content/themes/thetipsypig/images/reservations-trans.png");
  });
  $("#patio a img").hover(function(){
    $(this).attr("src", "../wp-content/themes/thetipsypig/images/patio-active-trans.png");
  },function(){
    $(this).attr("src", "../wp-content/themes/thetipsypig/images/patio-trans.png");
  });
  $("#shop a img").hover(function(){
    $(this).attr("src", "../wp-content/themes/thetipsypig/images/shop-active-trans.png");
  },function(){
    $(this).attr("src", "../wp-content/themes/thetipsypig/images/shop-trans.png");
  });
});
